package com.example.customviewtest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.example.utils.WifiAdmin;

/**
 * @author nfl
 * 
 */
public class WifiActivity extends Activity {

	private Button connect_bn;
	private WifiAdmin mWifiAdmin;
	private String wifiName;// WiFi的名称
	private String wifiPw;// WiFi的密码

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifi);
		initData();
		initView();
		setListeners();
	}

	private void setListeners() {
		connect_bn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mWifiAdmin.connectToWiFi()){
					Toast.makeText(WifiActivity.this, "连接成功", Toast.LENGTH_SHORT).show() ;
				}else{
					Toast.makeText(WifiActivity.this, "连接失败", Toast.LENGTH_SHORT).show() ;
				}
			}
		});
	}

	private void initData() {
		wifiName = ".的Mac mini";
		wifiPw = "1234567890";
		mWifiAdmin = new WifiAdmin(WifiActivity.this, wifiName, wifiPw);
	}

	public void initView() {
		connect_bn = (Button) findViewById(R.id.connect_bn);
	}

}