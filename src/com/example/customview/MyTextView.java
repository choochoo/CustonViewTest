package com.example.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.example.customviewtest.R;

public class MyTextView extends TextView {

	private int nick_name = -1;

	public MyTextView(Context context) {
		this(context, null);
	}

	public MyTextView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public MyTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray arr = getContext().obtainStyledAttributes(attrs,
				R.styleable.MyTextView);
		for (int i = 0; i < arr.length(); i++) {
			int index = arr.getIndex(i) ;
			switch (index) {
			case R.styleable.MyTextView_nick_name:
				nick_name = arr.getInt(index , -1);
				break;
			}
		}
		arr.recycle();
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		Log.i("NFL", "position;" + nick_name + ";width:" + getMeasuredWidth()
				+ ";height:" + getMeasuredHeight());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
	}

}
